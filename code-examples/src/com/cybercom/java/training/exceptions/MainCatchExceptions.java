package com.cybercom.java.training.exceptions;

public class MainCatchExceptions {

    public static void main(String[] args) {
        try {
            throwCheckedException();
        } catch (CheckedException e) {
            e.printStackTrace();
            throw new NullPointerException();
            //System.exit(1);
        } finally {
            System.out.println("finally");
        }
        System.out.println("po finally");

        //throwUncheckedException();
    }

    public static void throwCheckedException() throws CheckedException {
            throw new CheckedException();
    }

    public static void throwUncheckedException() {
        throw new UncheckedException();
    }
}
