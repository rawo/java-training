package com.cybercom.java.training.object;

public class ObjectExampleMain {

    public static void main(String[] args) {
        Object obj = new Object();
        obj.getClass();
        Object obj2 = new Object();

        System.out.println("obj.equals(obj2): " + obj.equals(obj2));
        System.out.println("obj.hashCode(): " + obj.hashCode());
        System.out.println("obj.toString(): " + obj.toString());

        Object toString = new ToString();
        System.out.println("GetName: "+toString.getClass().getName());

        if(toString instanceof ToString) {
            ToString toString2 = (ToString) toString;
            System.out.println("toString is instance of");
        }
        System.out.println("toString.toString(): " + toString.toString());

    }


}
