package com.cybercom.java.training.methods;

import java.math.BigDecimal;

public class MethodsWithAccessLevels {

    public Integer getInt() {
        return Integer.MAX_VALUE;
    }

    protected BigDecimal getBig() {
        return BigDecimal.TEN;
    }

    String getString() {
        return "bla";
    }

    private Long getLong() {
        return new Long(10L);
    }
}