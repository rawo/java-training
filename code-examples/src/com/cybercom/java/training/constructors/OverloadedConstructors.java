package com.cybercom.java.training.constructors;

import java.math.BigDecimal;

public class OverloadedConstructors {

    //default constructor
    public OverloadedConstructors() {

    }

    public OverloadedConstructors(String string, Integer integer) {

    }

    public OverloadedConstructors(String string, Long _long) {

    }

    protected OverloadedConstructors(BigDecimal decimal) {

    }

    private OverloadedConstructors(Long[] array) {

    }
}
