package com.cybercom.java.training.finals;

public class ClassExtendsFinalMethods extends ClassWithFinalMethods {


    //not allowed
//    public void overrideMe() {
//
//    }

    @Override
    public void overrideMeToo() {
        System.out.println("ClassWithFinalMethods#overrideMeToo");
    }

    private final void overrideMePrivate() {
        System.out.println("ClassWithFinalMethods#overrideMePrivate");
    }

    //not allowed
//    protected void overrideMeProtected() {
//
//    }
}