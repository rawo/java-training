package com.cybercom.java.training.finals;

public class ClassWithFinalMethods {

    public final void overrideMe() {
        System.out.println("ClassWithFinalMethods#overrideMe");
    }

    public void overrideMeToo() {
        System.out.println("ClassWithFinalMethods#overrideMeToo");
    }

    private final void overrideMePrivate() {
        System.out.println("ClassWithFinalMethods#overrideMePrivate");
    }

    final protected void overrideMeProtected() {
        System.out.println("ClassWithFinalMethods#overrideMeProtected");
    }
}
