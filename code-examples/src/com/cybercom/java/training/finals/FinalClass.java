package com.cybercom.java.training.finals;

import java.util.Date;

public final class FinalClass {
    final String finalString = "abc";
    final Date date = new Date();

    public FinalClass() {
        date.setYear(3);
        //date = new Date(); //not allowed
    }
}
