package com.cybercom.java.training.interfaces;

public interface Radio {
    FrequencyRanges getAvailableFrequencyRanges();
    RadioStream setFrequency(Frequency frequency);
}
