package com.cybercom.java.training.classfields.classextends;

import com.cybercom.java.training.classfields.ClassWithFields;

/**
 * Created by rafal.wokacz on 2016-02-25.
 */
public class ClassExtends extends ClassWithFields {

    public ClassExtends() {
        this.public_string = "";
        this.protected_string = "";
//        this.private_string = "";//no access
//        this.package_string = "";//no access

        ClassWithFields c = new ClassWithFields();
        c.public_string = "";
//        c.protected_string = ""; //no access
//        c.private_string = "";//no access
//        c.package_string = "";//no access

        ClassExtends c2 = new ClassExtends();
        c2.public_string = "";
        c2.protected_string = "";
//        c2.private_string = ""; //no access
//        c2.package_string = ""; //no access
    }
}
