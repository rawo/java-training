package com.cybercom.java.training.classfields;

public class UseClassWithFields {
    private ClassWithFields c;

    public UseClassWithFields() {
        c = new ClassWithFields();
        c.public_string = "";
        c.protected_string = "";
        //c.private_string = ""; //no access
        c.package_string = "";
    }
}