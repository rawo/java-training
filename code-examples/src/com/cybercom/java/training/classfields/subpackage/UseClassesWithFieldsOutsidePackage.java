package com.cybercom.java.training.classfields.subpackage;

import com.cybercom.java.training.classfields.ClassWithFields;

public class UseClassesWithFieldsOutsidePackage {
    private ClassWithFields c;

    public UseClassesWithFieldsOutsidePackage() {
        c = new ClassWithFields();
        c.public_string = "";
//        c.protected_string = ""; //no access
//        c.private_string = ""; //no access
//        c.package_string = ""; //no access
    }
}