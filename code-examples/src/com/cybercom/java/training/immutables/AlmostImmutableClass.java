package com.cybercom.java.training.immutables;

import java.util.Date;

public class AlmostImmutableClass {
    private final Date date;

    public AlmostImmutableClass(Date date) {
        this.date = date;
    }

    public Date getDate() {
        return date;
    }
}