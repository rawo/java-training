package com.cybercom.java.training.immutables;

import java.util.Date;

public class UseAlmostImmutableClass {
    public static void main(String[] args) {
        Date now = new Date();
        AlmostImmutableClass instance = new AlmostImmutableClass(now);
        System.out.println(instance.getDate());
        now.setYear(117);
        System.out.println(instance.getDate());
    }
}
