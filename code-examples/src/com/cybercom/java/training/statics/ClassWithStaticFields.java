package com.cybercom.java.training.statics;

public class ClassWithStaticFields {
    private Integer privateInt;

    public static final String FILE_PATH ="/etc/passwd";
    private static final String FILE_PATH2 ="/etc/groups";

    public static String public_static = new String("");

    protected static Integer protected_static = new Integer(10);

    static Long package_static = new Long(10L);

    private static Float private_static = new Float(1.2F);

    public  static void staticAccess(ClassWithStaticFields d) {
        d.privateInt = new Integer(20);
    }
}
