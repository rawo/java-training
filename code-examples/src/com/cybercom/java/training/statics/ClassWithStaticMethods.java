package com.cybercom.java.training.statics;

import java.io.*;

public class ClassWithStaticMethods {

    String readFileContents(File f) {
        String line;
        StringBuilder sb = new StringBuilder();
        try {
            BufferedReader fr = new BufferedReader(new FileReader(f));
            while((line = fr.readLine()) != null) {
                sb.append(line);
            }
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException(e);
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
        return sb.toString();
    }
}