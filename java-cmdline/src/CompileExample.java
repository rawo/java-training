import com.cybercom.java.training.compile.Dependency;

public class CompileExample {
	public static void main(String[] arg) {
		System.out.println("Start::main");
		Dependency dep = new Dependency("name");
		System.out.println("Dependency name: "+ dep.name);
	}
}
