package com.cybercom.java.training.gameoflife2;

public class GameOfLifeMain {

    public static final int BOARD_SIZE = 10;

    public static void main(String[] args) {
        Cell[][] board = new Cell[BOARD_SIZE][BOARD_SIZE];
        initBoard(board);
        
    }

    private static void initBoard(Cell[][] board) {
        board[1][1] = new Cell();
        board[1][2] = new Cell();
        board[2][2] = new Cell();
        board[2][1] = new Cell();
    }
}
