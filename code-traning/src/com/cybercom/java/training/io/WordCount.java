package com.cybercom.java.training.io;

import java.io.*;

public class WordCount {
    public static void main(String[] args) {
        BufferedReader br = null;

        try {
            String sCurrentLine = "";

            File inputFile = new File(System.getProperty("user.dir") + "\\code-traning\\src\\input.txt");
            br = new BufferedReader(new FileReader(inputFile));

            while ((sCurrentLine = br.readLine()) != null) {
                System.out.println(sCurrentLine);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (br != null) {
                    br.close();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }
}
