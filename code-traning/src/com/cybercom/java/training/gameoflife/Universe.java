package com.cybercom.java.training.gameoflife;

import java.util.Collection;

public interface Universe {
    void setCellAt(Cell alive, Coordinates coordinates);
    Collection<Coordinates> allCoordinates();

    Collection<Cell> getCells(Collection<Coordinates> neighbors);

    Cell getCell(Coordinates coordinates);

    boolean hasAliveCells();
}
