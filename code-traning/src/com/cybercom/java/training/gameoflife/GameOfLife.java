package com.cybercom.java.training.gameoflife;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public final class GameOfLife {
    private Universe universe;

    public GameOfLife(Universe universe) {

        this.universe = universe;
    }

    public static GameOfLife newGame(Universe universe) {
        return new GameOfLife(universe);
    }

    public void start() {
        while(universe.hasAliveCells()) {
            runPhase();
        }
    }

    private void runPhase() {
        Map<Coordinates, Cell> nextPhase = new HashMap<Coordinates,Cell>();
        for (Coordinates coordinates : universe.allCoordinates()) {
            System.out.println(coordinates.toString());
            Collection<Cell> neighborCells = universe.getCells(coordinates.neighbors());
            Cell currentCell = universe.getCell(coordinates);
            Cell newCell = GameOfLifeRules.isSatisfiedBy(currentCell, neighborCells);
            nextPhase.put(coordinates, newCell);
        }
        universe = UniverseCreator.from(nextPhase);
    }
}
