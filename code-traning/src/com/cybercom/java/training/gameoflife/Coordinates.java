package com.cybercom.java.training.gameoflife;

import java.util.Collection;

public interface Coordinates {
    Collection<Coordinates> neighbors();
}
