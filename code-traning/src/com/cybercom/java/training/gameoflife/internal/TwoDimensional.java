package com.cybercom.java.training.gameoflife.internal;


import com.cybercom.java.training.gameoflife.Coordinates;

import java.util.ArrayList;
import java.util.Collection;

public class TwoDimensional implements Coordinates {
    private final int x;
    private final int y;

    public TwoDimensional(int x, int y) {

        this.x = x;
        this.y = y;
    }


    @Override
    public String toString() {
        return "TwoDimensional{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }

    @Override
    public Collection<Coordinates> neighbors() {
        Collection<Coordinates> neighbors = new ArrayList<Coordinates>();
        neighbors.add(new TwoDimensional(x-1,y+1));
        neighbors.add(new TwoDimensional(x-1,y));
        neighbors.add(new TwoDimensional(x-1,y-1));

        neighbors.add(new TwoDimensional(x,y-1));
        neighbors.add(new TwoDimensional(x,y+1));

        neighbors.add(new TwoDimensional(x+1,y+1));
        neighbors.add(new TwoDimensional(x+1,y));
        neighbors.add(new TwoDimensional(x+1,y-1));

        return neighbors;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TwoDimensional that = (TwoDimensional) o;

        if (x != that.x) return false;
        return y == that.y;

    }

    @Override
    public int hashCode() {
        int result = x;
        result = 31 * result + y;
        return result;
    }
}
