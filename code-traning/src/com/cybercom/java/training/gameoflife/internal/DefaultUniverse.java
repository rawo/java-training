package com.cybercom.java.training.gameoflife.internal;

import com.cybercom.java.training.gameoflife.Cell;
import com.cybercom.java.training.gameoflife.Coordinates;
import com.cybercom.java.training.gameoflife.Universe;

import java.util.*;


public class DefaultUniverse implements Universe {
    private final Map<Coordinates, Cell> coordinates;

    public DefaultUniverse(Map<Coordinates, Cell> cellsWithCoordinates) {
        coordinates = cellsWithCoordinates;
    }

    public DefaultUniverse() {

        coordinates = new HashMap<Coordinates, Cell>();
    }

    @Override
    public void setCellAt(Cell cell, Coordinates coordinates) {
        this.coordinates.put(coordinates, cell);
    }

    @Override
    public Collection<Coordinates> allCoordinates() {
        return coordinates.keySet();
    }

    @Override
    public Collection<Cell> getCells(Collection<Coordinates> neighbors) {
        List<Cell> cells = new ArrayList<Cell>();
        Iterator<Coordinates> iterator = neighbors.iterator();
        while (iterator.hasNext()) {
            Coordinates next = iterator.next();
            Cell cell = coordinates.get(next);
            if (cell == null) {
                cell = Cell.DEAD;
            }
            cells.add(cell);
        }
        return cells;
    }

    @Override
    public Cell getCell(Coordinates coordinates) {
        Cell cell = this.coordinates.get(coordinates);
        return cell == null ? Cell.DEAD : cell;
    }

    @Override
    public boolean hasAliveCells() {
        for (Cell cell : coordinates.values()) {
            if(cell == Cell.ALIVE) {
                return true;
            }
        }
        return false;
    }
}