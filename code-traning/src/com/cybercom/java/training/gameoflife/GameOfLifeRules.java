package com.cybercom.java.training.gameoflife;

import java.util.Collection;

public class GameOfLifeRules {
    public static Cell isSatisfiedBy(Cell currentCell, Collection<Cell> neighborCells) {
        int numberOfAliveNeighbors = countAliveNeighbors(neighborCells);
        boolean cellIsAlive = currentCell == Cell.ALIVE;

        if (cellIsAlive && numberOfAliveNeighbors < 2) {
            return Cell.DEAD;
        }

        if (cellIsAlive && (numberOfAliveNeighbors == 2 || numberOfAliveNeighbors == 3)) {
            return Cell.ALIVE;
        }

        if (cellIsAlive && numberOfAliveNeighbors > 3) {
            return Cell.DEAD;
        }

        if (!cellIsAlive && numberOfAliveNeighbors == 3) {
            return Cell.ALIVE;
        }

        return Cell.DEAD;
    }

    private static int countAliveNeighbors(Collection<Cell> neighborCells) {
        int counter = 0;
        for (Cell neighborCell : neighborCells) {
            if (neighborCell == Cell.ALIVE) {
                counter++;
            }
        }
        return counter;
    }
}
