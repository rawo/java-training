package com.cybercom.java.training.gameoflife;

import com.cybercom.java.training.gameoflife.internal.TwoDimensional;

public class GameOfLifeMain {
    public static void main(String[] args) {
        Universe universe = UniverseCreator.create();

        Coordinates coordinates = new TwoDimensional(1, 2);
        universe.setCellAt(Cell.ALIVE, coordinates);

        coordinates = new TwoDimensional(1, 1);
        universe.setCellAt(Cell.ALIVE, coordinates);

        GameOfLife game = GameOfLife.newGame(universe);
        game.start();
    }
}