package com.cybercom.java.training.gameoflife;


import com.cybercom.java.training.gameoflife.internal.DefaultUniverse;

import java.util.Map;

public final class UniverseCreator {
    public static Universe create() {
        return new DefaultUniverse();
    }

    public static Universe from(Map<Coordinates, Cell> cellsWithCoordinates) {
        return new DefaultUniverse(cellsWithCoordinates);
    }
}
