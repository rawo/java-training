package com.cybercom.java.training.interfaces;

import java.util.HashMap;
import java.util.Map;

public class HashMapExample {

    public static void main(String[] args) {
        Map cityPopulation = new HashMap();

        cityPopulation.put("Warszawa", 94939204);
        cityPopulation.put("Lodz", 3923098);
        cityPopulation.put("Wroclaw", 3923244);
        cityPopulation.put("Krakow", 1093923);

        System.out.println("Total cityPopulation: " + cityPopulation.size());

        for (Object key : cityPopulation.keySet())
            System.out.println(key + " - " + cityPopulation.get(key));
        System.out.println();

        String searchKey = "Lodz";
        if (cityPopulation.containsKey(searchKey))
            System.out.println("Population for " + searchKey + " is " + cityPopulation.get(searchKey) + "\n");

        cityPopulation.clear();

        System.out.println("After clear operation, size: " + cityPopulation.size());
    }
}
