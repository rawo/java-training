package com.cybercom.java.training.interfaces;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class SortList {
    public static void main(String[] args) {
        List<Integer> integerList = new ArrayList<Integer>();

        Comparator<Integer> myIntegerComparator = new MyIntegerComparator();
        Collections.sort(integerList,myIntegerComparator);
    }
}
